-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Máquina: localhost
-- Data de Criação: 31-Ago-2015 às 08:01
-- Versão do servidor: 5.5.38-0ubuntu0.14.04.1
-- versão do PHP: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `scrum`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `fase`
--

CREATE TABLE IF NOT EXISTS `fase` (
  `id_fase` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) CHARACTER SET latin1 NOT NULL,
  `descricao` text CHARACTER SET latin1 NOT NULL,
  `inicio` date NOT NULL,
  `fim` date NOT NULL,
  `id_projeto` int(11) NOT NULL,
  PRIMARY KEY (`id_fase`),
  KEY `fases_ibfk_1` (`id_projeto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `grupo`
--

CREATE TABLE IF NOT EXISTS `grupo` (
  `id_grupo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) CHARACTER SET latin1 NOT NULL,
  `participantes` int(11) NOT NULL,
  `descricao` varchar(300) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id_grupo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `missao`
--

CREATE TABLE IF NOT EXISTS `missao` (
  `id_missao` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) CHARACTER SET latin1 NOT NULL,
  `descricao` text CHARACTER SET latin1 NOT NULL,
  `inicio` date DEFAULT NULL,
  `fim` date DEFAULT NULL,
  `id_fase` int(11) NOT NULL,
  PRIMARY KEY (`id_missao`),
  KEY `id_fase` (`id_fase`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `papel`
--

CREATE TABLE IF NOT EXISTS `papel` (
  `id_papel` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `descricao` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_papel`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Extraindo dados da tabela `papel`
--

INSERT INTO `papel` (`id_papel`, `nome`, `descricao`) VALUES
(1, 'aluno', ' participante do projeto'),
(2, 'lider', 'lider do grupo, pode variar'),
(3, 'professor', 'professor da turma, tem acesso a todos os projetos'),
(4, 'criador', 'criador do projeto'),
(5, 'administrador', 'adminstrador do sistema'),
(6, 'usuario', 'espera receber um papel do admin'),
(7, 'delegar_aluno', 'aluno esperando autorização para acessar o sistema'),
(8, 'delegar_professor', 'professor esperando autorização para acessar o sistema');

-- --------------------------------------------------------

--
-- Estrutura da tabela `papel_projeto`
--

CREATE TABLE IF NOT EXISTS `papel_projeto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `id_papel` int(11) NOT NULL,
  `id_projeto` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_usuario` (`id_usuario`,`id_papel`,`id_projeto`),
  KEY `fk_projeto` (`id_projeto`),
  KEY `fk_papel` (`id_papel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `papel_sistema`
--

CREATE TABLE IF NOT EXISTS `papel_sistema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `id_papel` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_usuario` (`id_usuario`),
  KEY `id_papel` (`id_papel`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Extraindo dados da tabela `papel_sistema`
--

INSERT INTO `papel_sistema` (`id`, `id_usuario`, `id_papel`) VALUES
(1, 1, 5),
(2, 2, 1),
(3, 3, 1),
(4, 5, 3),
(6, 7, 8),
(9, 9, 7),
(11, 10, 1),
(13, 13, 1),
(14, 11, 7),
(15, 12, 3),
(16, 8, 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `projeto`
--

CREATE TABLE IF NOT EXISTS `projeto` (
  `id_projeto` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) CHARACTER SET latin1 NOT NULL,
  `descricao` text CHARACTER SET latin1 NOT NULL,
  `inicio` date NOT NULL,
  `fim` date NOT NULL,
  PRIMARY KEY (`id_projeto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tarefa`
--

CREATE TABLE IF NOT EXISTS `tarefa` (
  `id_tarefa` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) CHARACTER SET latin1 NOT NULL,
  `descricao` text CHARACTER SET latin1,
  `estado` int(5) NOT NULL DEFAULT '1',
  `inicio` date NOT NULL,
  `fim` date NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_missao` int(11) NOT NULL,
  PRIMARY KEY (`id_tarefa`),
  UNIQUE KEY `id_usuario` (`id_usuario`),
  KEY `tarefas_ibfk_1` (`id_missao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET latin1 NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `nome` varchar(100) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `username`, `password`, `nome`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin'),
(2, 'pedro', 'c6cc8094c2dc07b700ffcc36d64e2138', 'pedro'),
(3, 'ana', '276b6c4692e78d4799c12ada515bc3e4', 'ana'),
(5, 'artur', '2846bbb84da0bc71b222a9343ded1474', 'artur'),
(7, 'raul', 'bc7a844476607e1a59d8eb1b1f311830', 'raul'),
(8, 'teste', '698dc19d489c4e4db73e28a713eab07b', 'teste'),
(9, 'lalala', '81dc9bdb52d04dc20036dbd8313ed055', 'lalala'),
(10, 'lia', '8d84dd7c18bdcb39fbb17ceeea1218cd', 'lia'),
(11, 'novo', '42323e3211ed4478b2b8ba87d4185a03', 'novo'),
(12, 'tomas', '4b506c2968184be185f6282f5dcac238', 'tomas'),
(13, 'aÃ§lskdfjaÃ§slkdfj', 'e10adc3949ba59abbe56e057f20f883e', 'ajfsdÃ§akjfÃ§sdlkd');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `fase`
--
ALTER TABLE `fase`
  ADD CONSTRAINT `fase_ibfk_1` FOREIGN KEY (`id_projeto`) REFERENCES `projeto` (`id_projeto`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `missao`
--
ALTER TABLE `missao`
  ADD CONSTRAINT `missao_ibfk_1` FOREIGN KEY (`id_fase`) REFERENCES `fase` (`id_fase`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `papel_projeto`
--
ALTER TABLE `papel_projeto`
  ADD CONSTRAINT `fk_papel` FOREIGN KEY (`id_papel`) REFERENCES `papel` (`id_papel`),
  ADD CONSTRAINT `fk_projeto` FOREIGN KEY (`id_projeto`) REFERENCES `projeto` (`id_projeto`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `papel_sistema`
--
ALTER TABLE `papel_sistema`
  ADD CONSTRAINT `papel_sistema_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`),
  ADD CONSTRAINT `papel_sistema_ibfk_2` FOREIGN KEY (`id_papel`) REFERENCES `papel` (`id_papel`);

--
-- Limitadores para a tabela `tarefa`
--
ALTER TABLE `tarefa`
  ADD CONSTRAINT `tarefa_ibfk_1` FOREIGN KEY (`id_missao`) REFERENCES `missao` (`id_missao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tarefa_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
